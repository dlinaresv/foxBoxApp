# **Instructions:**

> Note: Previous to run the project either android or ios, you must have
> configured the environment for React Native. Please follow this steps
> and come again to run the project.
> https://reactnative.dev/docs/environment-setup

## Running project

To run the project you can use this commands:

For Android:

    yarn run android

For iOS:

    yarn run ios

There you can see a Screen with an input where you can enter the value that you want. And when you click the button, you will see the result of the nGram algorithm.

![enter image description here](https://drive.google.com/uc?export=view&id=1AGag1Gl7JuKjl1lcTD-X_h_kU37W5sac)

## Running the tests

To run the test of the function, you can run the next command inside the project:

    yarn run test
