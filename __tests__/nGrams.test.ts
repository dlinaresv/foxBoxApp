import nGrams from '../src/util';

describe('Testing nGram function', () => {
  test('show me the code test', () => {
    const expected: string[] = [
      'Show',
      'Show me',
      'Show me the',
      'Show me the code',
      'me',
      'me the',
      'me the code',
      'the',
      'the code',
      'code',
    ];
    expect(nGrams('Show me the code.')).toEqual(expected);
  });

  test('validate null string', () => {
    const expected: string[] = [];
    expect(nGrams(null)).toEqual(expected);
  });

  test('validate empty string', () => {
    const expected: string[] = [];
    expect(nGrams('')).toEqual(expected);
  });

  test('validate input numbers', () => {
    const expected: string[] = [];
    expect(nGrams(45689)).toEqual(expected);
  });

  test('validate special characters', () => {
    const expected: string[] = [];
    const input = '#/&_-*.';
    expect(nGrams(input)).toEqual(expected);
  });

  test('validate string with numbers and special chars', () => {
    const expected: string[] = [
      'show',
      'show me',
      'show me 43',
      'show me 43 the',
      'show me 43 the code',
      'show me 43 the code 123',
      'me',
      'me 43',
      'me 43 the',
      'me 43 the code',
      'me 43 the code 123',
      '43',
      '43 the',
      '43 the code',
      '43 the code 123',
      'the',
      'the code',
      'the code 123',
      'code',
      'code 123',
      '123',
    ];
    const input = 'show me %43- the code 123';
    expect(nGrams(input)).toEqual(expected);
  });
});
