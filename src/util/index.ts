const nGrams = (string: string | null | number): string[] => {
  const resultArray: string[] = [];
  if (!string || typeof string !== 'string') {
    return resultArray;
  }
  string = string.replace(/[^a-zA-Z0-9\s]/gi, '');
  const splitString = string
    .trim()
    .split(' ')
    .filter(n => n);
  for (let i = 0; i < splitString.length; i++) {
    const nGramLevel = [];
    for (let j = i; j < splitString.length; j++) {
      const newGram: string = nGramLevel[j - i - 1]
        ? `${nGramLevel[j - i - 1]} ${splitString[j]}`
        : splitString[j];
      nGramLevel.push(newGram.trim());
    }
    resultArray.push(...nGramLevel);
  }
  return resultArray.filter(n => n);
};

export default nGrams;
