/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useCallback, useState} from 'react';
import {
  Button,
  FlatList,
  ListRenderItem,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import nGrams from './src/util';

const App = () => {
  const [inputValue, setInputValue] = useState('');
  const [nGramsResult, setNGramsResult] = useState<string[] | null>(null);
  const renderRow: ListRenderItem<any> = ({item}) => {
    return (
      <View>
        <Text>{item}</Text>
      </View>
    );
  };

  const generateNGrams = useCallback(() => {
    const result = nGrams(inputValue);
    setNGramsResult(result);
  }, [inputValue]);

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <TextInput
          style={styles.inputText}
          value={inputValue}
          onChangeText={setInputValue}
        />
        <TouchableOpacity
          style={styles.button}
          activeOpacity={0.5}
          onPress={() => generateNGrams()}>
          <Text style={styles.textButton}>Generate nGrams</Text>
        </TouchableOpacity>
        {nGramsResult &&
          (nGramsResult.length ? (
            <FlatList data={nGramsResult} renderItem={renderRow} />
          ) : (
            <Text style={styles.errorLabel}>
              Por favor ingrese letras y números
            </Text>
          ))}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    paddingTop: 150,
  },
  inputText: {
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 15,
    width: 300,
    height: 50,
    borderRadius: 10,
    borderColor: '#cfcfcf',
  },
  button: {
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 15,
    width: 200,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#cfcfcf',
    backgroundColor: '#71a2f0',
    margin: 10,
    display: 'flex',
  },
  textButton: {
    color: 'white',
    fontWeight: '600',
    fontSize: 18,
  },
  errorLabel: {
    color: '#c8523d',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 20,
  },
});

export default App;
